using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviourPunCallbacks
{
    public GameObject[] playerPrefab;
    public static GameManager instance = null;
    public Transform[] spawnPoints, teamASpawnPoints, teamBSpawnPoints;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    
    void Start()
    {
        if(PhotonNetwork.IsConnectedAndReady)
        {
            int randomIndex = Random.Range(0, spawnPoints.Length);
            object playerSelectionNumber;

            if(PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                GameObject player = PhotonNetwork.Instantiate(playerPrefab[(int)playerSelectionNumber].name, new Vector3(0, 0, 0), Quaternion.identity);
                SpawnPlayer(player);
            }
        }
    }

    public void SpawnPlayer(GameObject player)
    {
        int randomIndex = Random.Range(0, spawnPoints.Length-1);
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("tdm"))
        {   
            randomIndex = Random.Range(0, teamASpawnPoints.Length);
            if(player.gameObject.CompareTag("TeamA"))
            {
                player.transform.position = new Vector3 (teamASpawnPoints[randomIndex].position.x, 0 ,teamASpawnPoints[randomIndex].position.z);
            }
            else if (player.gameObject.CompareTag("TeamB"))
            {
                player.transform.position = new Vector3 (teamBSpawnPoints[randomIndex].position.x, 0 ,teamBSpawnPoints[randomIndex].position.z);
            }
        }

        else
        {
            player.transform.position = new Vector3 (spawnPoints[randomIndex].position.x, 0 ,spawnPoints[randomIndex].position.z);
        }
    }
}
