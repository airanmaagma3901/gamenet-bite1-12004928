using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject playerUIPrefab;
    public TextMeshProUGUI playerNameText;
    public GameObject fpsModel;
    public GameObject nonFpsModel;
    private Animator animator;
    public Avatar fpsAvatar, nonFpsAvatar;

    void Awake()
    {
        if (photonView.IsMine)
        {
            GameObject playerUI = Instantiate(playerUIPrefab);
        }

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("tdm"))
        {
            //First player is actor number 1; So Team A is odd, Team B is even 
            if (photonView.Owner.ActorNumber % 2 != 0)
            {
                this.gameObject.tag = "TeamA";
            }
            else
            {
                this.gameObject.tag = "TeamB";
            }
        }

        camera.enabled = photonView.IsMine;

        //Player animations
        animator = this.GetComponent<Animator>();
        animator.SetBool("isLocalPlayer", photonView.IsMine);
        animator.avatar = photonView.IsMine ? fpsAvatar : nonFpsAvatar;
        fpsModel.SetActive(photonView.IsMine);
        nonFpsModel.SetActive(!photonView.IsMine);

        //Player Movement and Shooting
        GetComponent<RigidbodyFirstPersonController>().enabled = photonView.IsMine;
        GetComponent<PlayerShooting>().enabled = photonView.IsMine;
        
        //UI
        playerNameText.text = photonView.Owner.NickName;
    }
}
