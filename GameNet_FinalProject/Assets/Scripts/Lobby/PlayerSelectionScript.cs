using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSelectionScript : MonoBehaviour
{
    public GameObject[] selectablePlayers;
    public int playerSelectionNumber;
    
    void Start()
    {
        playerSelectionNumber = 0;
        ActivatePlayer(playerSelectionNumber);
    }

    public void ActivatePlayer(int x)
    {
        foreach(GameObject go in selectablePlayers)
        {
            go.SetActive(false);
        }

        selectablePlayers[x].SetActive(true);

        //Setting the player selection for the character
        ExitGames.Client.Photon.Hashtable playerSelectionProperties = new ExitGames.Client.Photon.Hashtable(){ {Constants.PLAYER_SELECTION_NUMBER, playerSelectionNumber} };
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerSelectionProperties);
    }

    public void GoToNextPlayer()
    {
        playerSelectionNumber++;

        if(playerSelectionNumber >= selectablePlayers.Length)
        {
            playerSelectionNumber = 0;
        }

        ActivatePlayer(playerSelectionNumber);
    }

    public void GoToPreviousPlayer()
    {
        playerSelectionNumber--;

        if(playerSelectionNumber < 0)
        {
            playerSelectionNumber = selectablePlayers.Length - 1;
        }

        ActivatePlayer(playerSelectionNumber);
    }
}
