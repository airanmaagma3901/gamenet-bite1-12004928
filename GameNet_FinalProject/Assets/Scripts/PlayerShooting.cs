using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerShooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    private GameManager gameManager;
    private GameObject ammoCountText, killFeedText, healthPointText, respawnText, playerNameTextUI, teamText, teamAKillCountText, teamBKillCountText, soloKillCountText;

    [Header("HP Related Stuff")]
    public float startHealth;
    public float health;
    public Image healthBar;
    private Animator animator;
    private bool isAlive = true;

    [Header("Stats")]
    private static int teamAKillCount, teamBKillCount;
    private int soloKillCount;

    [Header("Weapon Attributes")]
    private bool canShoot, isReloading, canFire = true;
    public GameObject hitEffectPrefab;
    private int weaponType; //0 - Shotgun, 1 - Pistol, 2 - Assault Rifle
    private int ammoCount;
    private int[] maxAmmoCount = new int[3];
    private int[] weaponDamage = new int[3];
    private int[] weaponRange = new int[3];
    private int[] reloadTime = new int[3];
    private float[] fireRate = new float[3];

    int playerClass; 
    object playerSelectionNumber;
    
    void Start()
    {
        if(PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
        playerClass = (int)playerSelectionNumber;
        weaponType = playerClass;

        gameManager = FindFirstObjectByType<GameManager>();
        animator = this.GetComponent<Animator>();
        //Weapon Stats
        maxAmmoCount[0] = 5;
        maxAmmoCount[1] = 12;
        maxAmmoCount[2] = 30;
        ammoCount = maxAmmoCount[weaponType];

        weaponDamage[0] = 50;
        weaponDamage[1] = 12;
        weaponDamage[2] = 7;

        weaponRange[0] = 20;
        weaponRange[1] = 40;
        weaponRange[2] = 80;

        reloadTime[0] = 3;
        reloadTime[1] = 1;
        reloadTime[2] = 5;

        fireRate[0] = 0.5f;
        fireRate[1] = 0.3f;
        fireRate[2] = 0.1f;

        //Health Stats
        health = startHealth;
        healthBar.fillAmount = health / startHealth;

        //Player Stats
        ammoCountText = GameObject.Find("AmmoCount");
        healthPointText = GameObject.Find("HealthPoints");
        playerNameTextUI = GameObject.Find("PlayerNameUI");
        teamText = GameObject.Find("TeamName");

        //Kills
        soloKillCountText = GameObject.Find("Kills");
        soloKillCountText.gameObject.GetComponent<TextMeshProUGUI>().text = "Kills: " + soloKillCount.ToString();

        teamAKillCountText = GameObject.Find("TeamAKills");
        teamBKillCountText = GameObject.Find("TeamBKills");

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("tdm"))
        {
            teamAKillCountText.gameObject.GetComponent<TextMeshProUGUI>().text = "Team A Kills: " + teamAKillCount.ToString();
            teamBKillCountText.gameObject.GetComponent<TextMeshProUGUI>().text = "Team B Kills: " + teamBKillCount.ToString();
        }
            
        //UI
        ammoCountText.GetComponent<TextMeshProUGUI>().text = "Ammo: " + ammoCount.ToString();
        healthPointText.GetComponent<TextMeshProUGUI>().text = "Health: " + this.health.ToString();
        playerNameTextUI.GetComponent<TextMeshProUGUI>().text = "Name: " + photonView.Owner.NickName;
        teamText.GetComponent<TextMeshProUGUI>().text = "|" + this.gameObject.tag + "|";
        
    }

    void Update()
    {
        healthPointText.GetComponent<TextMeshProUGUI>().text = "Health: " + this.health.ToString();
        soloKillCountText.gameObject.GetComponent<TextMeshProUGUI>().text = "Kills: " + soloKillCount.ToString();

        //Shoots and Deducts Ammo
        if(Input.GetKey(KeyCode.Mouse0) && ammoCount > 0 && isAlive && canFire)
        {
            ammoCount--;
            ammoCountText.GetComponent<TextMeshProUGUI>().text = "Ammo: " + ammoCount.ToString();
            StartCoroutine(Fire(weaponDamage[weaponType], weaponRange[weaponType], fireRate[weaponType]));
            canFire = false;
        }

        //Manual or Automatic Reloading
        if(Input.GetKeyDown(KeyCode.R) || ammoCount == 0 && !isReloading)
        {
            StartCoroutine(Reload(reloadTime[weaponType]));
        }
    }

    IEnumerator Fire(int weaponDamage, int weaponRange, float waitTime)
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        if (Physics.Raycast(ray, out hit , weaponRange))
        {
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);
            
            if (!hit.collider.gameObject.CompareTag(this.gameObject.tag))
            {
                Debug.Log("Hit");
                if (hit.collider.gameObject.CompareTag("TeamA") || hit.collider.gameObject.CompareTag("TeamB"))
                {
                    if(hit.collider.gameObject.GetComponent<PlayerShooting>().isAlive && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
                    {
                        hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, weaponDamage);

                        if (!hit.collider.gameObject.GetComponent<PlayerShooting>().isAlive)
                        {
                            this.photonView.RPC("AddKillCount", RpcTarget.AllBuffered);
                        }
                    }
                }
            }

            if(hit.collider.gameObject.CompareTag("Player"))
            {
                if(hit.collider.gameObject.GetComponent<PlayerShooting>().isAlive && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
                {
                    hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, weaponDamage);

                    if (!hit.collider.gameObject.GetComponent<PlayerShooting>().isAlive)
                    {
                        this.photonView.RPC("AddKillCount", RpcTarget.AllBuffered);
                    }
                }
            }
        }

        yield return new WaitForSeconds(waitTime);
        
        canFire = true;
    }

    IEnumerator Reload(int reloadTime)
    {
        isReloading = true;

        yield return new WaitForSeconds(reloadTime);

        isReloading = false;
        ammoCount = maxAmmoCount[weaponType];
        ammoCountText.GetComponent<TextMeshProUGUI>().text = "Ammo: " + ammoCount.ToString();
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die()
    {
        if(photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown()
    {
        respawnText = GameObject.Find("Respawn");
        float respawnTime = 5.0f;

        while(respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<RigidbodyFirstPersonController>().enabled = false;
            respawnText.GetComponent<TextMeshProUGUI>().text = "You are killed. Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<TextMeshProUGUI>().text = "";

        gameManager.SpawnPlayer(this.gameObject);

        transform.GetComponent<RigidbodyFirstPersonController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }
    
    [PunRPC]
    public void RegainHealth()
    {
        this.GetComponent<PlayerShooting>().isAlive = true;
        this.health = startHealth;
        this.healthBar.fillAmount = health / startHealth;
    }

   [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        healthPointText = GameObject.Find("HealthPoints");
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            health = 0;
            this.GetComponent<PlayerShooting>().isAlive = false;
            StartCoroutine(WhoKilledWho(info));
            Die();
        }
    }

    IEnumerator WhoKilledWho(PhotonMessageInfo info)
    {
        killFeedText = GameObject.Find("KillFeed");
        killFeedText.GetComponent<TextMeshProUGUI>().text = (info.Sender.NickName + " killed " + info.photonView.Owner.NickName);

        yield return new WaitForSeconds(5.0f);

        killFeedText.GetComponent<TextMeshProUGUI>().text = "";
    }
    
    [PunRPC]
    public void AddKillCount()
    {
        teamAKillCountText = GameObject.Find("TeamAKills");
        teamBKillCountText = GameObject.Find("TeamBKills");
        
        soloKillCount++;
        if (this.CompareTag("TeamA"))
        {
            teamAKillCount++;
        }
        else if (this.CompareTag("TeamB"))
        {
            teamBKillCount++;
        }
 
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("tdm"))
        {
            teamAKillCountText.gameObject.GetComponent<TextMeshProUGUI>().text = "Team A Kills: " + teamAKillCount.ToString();
            teamBKillCountText.gameObject.GetComponent<TextMeshProUGUI>().text = "Team B Kills: " + teamBKillCount.ToString();
        }

        if(teamBKillCount == 5 || teamAKillCount == 5)
        {
            TDMGameOver();
        }

        if (soloKillCount == 5 && teamAKillCount == 0 && teamBKillCount == 0)
        {
            FFAGameOver();
        }
    }

    public void TDMGameOver()
    {
        GameObject winnerText = GameObject.Find("Winner");
        winnerText.GetComponent<TextMeshProUGUI>().text = this.gameObject.tag + " is the winner!";
    }
    
    public void FFAGameOver()
    {
        GameObject winnerText = GameObject.Find("Winner");
        winnerText.GetComponent<TextMeshProUGUI>().text = this.photonView.Owner.NickName + " is the winner!";
    }
}