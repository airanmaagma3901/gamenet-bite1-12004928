using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class ScoreTracker : MonoBehaviourPunCallbacks
{
    public static int teamAKillCount;
    public static int teamBKillCount;
    public static int soloKillCount;
    private GameObject teamAKillCountText, teamBKillCountText, soloKillCountText;
    private GameObject winnerText;

    void Start()
    {
        teamAKillCountText = GameObject.Find("TeamAKills");
        teamBKillCountText = GameObject.Find("TeamBKills");
        soloKillCountText = GameObject.Find("Kills");

        teamAKillCountText.GetComponent<TextMeshProUGUI>().text = "Team A kills: 0";
        teamBKillCountText.GetComponent<TextMeshProUGUI>().text = "Team B kills: 0";
        soloKillCountText.GetComponent<TextMeshProUGUI>().text = "Kills: 0 ";
    }

    [PunRPC]
    public void AddKillCount()
    {
        Debug.Log(photonView.Owner.NickName + " got a kill!");
        if (this.CompareTag("TeamA"))
        {
            teamAKillCount++;
        }
        else if (this.CompareTag("TeamB"))
        {
            teamBKillCount++;
        }
        else if (this.CompareTag("Player"))
        {
            soloKillCount++;
        }

        soloKillCountText.gameObject.GetComponent<TextMeshProUGUI>().text = "Kills: " + soloKillCount.ToString();
        teamAKillCountText.gameObject.GetComponent<TextMeshProUGUI>().text = "Team A Kills: " + teamAKillCount.ToString();
        teamBKillCountText.gameObject.GetComponent<TextMeshProUGUI>().text = "Team B Kills: " + teamBKillCount.ToString();

        if(teamBKillCount == 5 || teamAKillCount == 5)
        {
            TDMGameOver();
        }

        if (soloKillCount == 5)
        {
            FFAGameOver();
        }
    }

    public void TDMGameOver()
    {
        GameObject winnerText = GameObject.Find("Winner");
        winnerText.GetComponent<TextMeshProUGUI>().text = this.gameObject.tag + " is the winner!";
    }
    public void FFAGameOver()
    {
        GameObject winnerText = GameObject.Find("Winner");
        winnerText.GetComponent<TextMeshProUGUI>().text = this.photonView.Owner.NickName + " is the winner!";
    }
}