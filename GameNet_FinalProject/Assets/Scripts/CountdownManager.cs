using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class CountdownManager : MonoBehaviourPunCallbacks
{
    public TextMeshProUGUI timerText;
    public float timeToStartRace = 5.0f;
    string gameMode;
    
    void Start()
    {
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("ffa"))
        {
            //timerText = FFAGameManager.instance.timeText;
            gameMode = "StartFFA";
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("tdm"))
        {
           //timerText = TDMGameManager.instance.timeText;
            gameMode = "StartTDM";
        }
    }
    public void Update()
    {
        if(PhotonNetwork.IsMasterClient)
        {
            if (timeToStartRace > 0)
            {
                timeToStartRace -= Time.deltaTime;
                photonView.RPC("SetTime",RpcTarget.AllBuffered, timeToStartRace);
            }
            if (timeToStartRace < 0)
            {
                StartGame(gameMode);
            }
        }
    }

    public void StartGame(string gm)
    {
        photonView.RPC(gm, RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void SetTime(float time)
    {
        if (time > 0 )
        {
            timerText.text = time.ToString("F1");
        }
        else
        {
            timerText.text = "";
        }
    }
    
    [PunRPC]
    public void StartFFA()
    {
        //GetComponent<VehicleMovement>().isControlEnabled =  true;
        this.enabled = false;
    }

    [PunRPC]
    public void StartTDM()
    {
        //GetComponent<VehicleMovement>().isControlEnabled =  true;
        //GetComponent<VehicleCombat>().canShoot = true;
        this.enabled = false;
    }

}
