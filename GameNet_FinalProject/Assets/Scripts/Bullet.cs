using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Bullet : MonoBehaviourPunCallbacks
{
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            //Debug.Log("Hit " + col.gameObject.GetComponent<PlayerSetup>().playerNameText.text);
            //col.gameObject.GetComponent<VehicleCombat>().GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
            Destroy(this.gameObject);
        }

        if (col.gameObject.CompareTag("Wall"))
        {
            Destroy(this.gameObject);
        }
    }
}